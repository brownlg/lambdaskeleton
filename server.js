// AWS API Gateway modelled as express server:
var express = require('express')
    , bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());


// AWS Lambda functions ----------------------------------------------------------------
// add your functions here:
var lambdaInterface = require("./lambdaFunctions/chatBot.js");


// -------------------------------------------------------------------------------------


// AWS gateway resources & Methods -----------------------------------------------------
// add your resources & methodds here:

app.post('/', function(request, response){
    console.log(request.body);
    lambdaInterface.handler(request.body, "", function(unknown, data){
        response.json(data);
    });
});


app.post('/webhook', function(request, response){
    lambdaInterface.handler(request.body, "", function(unknown, data){
        response.json(data);
    });
});



// --------------------------------------------------------------------------------------



app.listen(3000);

